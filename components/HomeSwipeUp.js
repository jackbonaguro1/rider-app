import React, { useState, useMemo, useRef, useEffect } from 'react';
import {
  View,
  ScrollView,
  Dimensions,
  Animated,
  PanResponder
} from "react-native";
import Text from './Text';
import styles, { palette } from "../styles";
import ButtonInput from "./ButtonInput";
import { connect } from "react-redux";
// import mapboxgl from 'mapbox-gl/dist/mapbox-gl.js';
// mapboxgl.accessToken = 'pk.eyJ1IjoiamFja2JvbmFndXJvIiwiYSI6ImNrbGl2am5udTFvZnMyb282NDNhOGg2ZG0ifQ.KfiapHMvzGdexBSTtCg_Aw';
import mapboxGeocode from '@mapbox/mapbox-sdk/services/geocoding';
import { SolidIcons } from "react-native-fontawesome";
const geocodeService = mapboxGeocode({ accessToken: 'pk.eyJ1IjoiamFja2JvbmFndXJvIiwiYSI6ImNrbGl2am5udTFvZnMyb282NDNhOGg2ZG0ifQ.KfiapHMvzGdexBSTtCg_Aw' });

const useMaximizer = (destinationRef) => {
  // Discrete maximize & minimize actions
  const [swipeLength, _] = useState(new Animated.Value(0));
  const [isMaximized, setMaximized] = useState(false);

  const maximize = () => {
    setMaximized(true);
    Animated.spring(swipeLength, { toValue: 1, stiffness: 260, damping: 20, useNativeDriver: false }).start();
    try {
      destinationRef.current.focus();
    } catch (e) {
      console.error(e);
    }
  }
  const minimize = () => {
    Animated.spring(swipeLength, { toValue: 0, stiffness: 260, damping: 20, useNativeDriver: false }).start();
    setMaximized(false);
    try {
      destinationRef.current.blur();
    } catch (e) {
      console.error(e);
    }
  }

  return [swipeLength, isMaximized, maximize, minimize];
};

const useSwipe = (height, maximizer) => {
  // Gesture-driven maximize
  const [swipeLength, isMaximized, maximize, minimize] = maximizer;
  const panResponder = useMemo(() => PanResponder.create({
    onMoveShouldSetPanResponder: (evt, gestureState) => {
      const threshold = isMaximized ? 0 : height / 2;
      const inBounds = evt.nativeEvent.pageY > (threshold);

      const {dx, dy} = gestureState;
      const notTap = (Math.abs(dx) > 20) || (Math.abs(dy) > 20);

      return inBounds && notTap;
    },
    onPanResponderMove: (evt, gestureState) => {
      const normalized = isMaximized ?
        1 - (gestureState.dy / (height / 2)) : // Normalize to 0..1
        - (gestureState.dy / (height / 2)); // Normalize to 0..1
      swipeLength.setValue(normalized);
    },
    onPanResponderTerminationRequest: (evt, gestureState) => true,
    onStartShouldSetPanResponderCapture: (evt, gestureState) => false,
    onPanResponderRelease: (evt, gestureState) => {
      const normalized = - gestureState.dy / height; // Normalize to 0..1
      if (normalized < 0.25) {
        minimize();
      } else {
        maximize();
      }
    },
    onPanResponderTerminate: (evt, gestureState) => {
      if (isMaximized) {
        maximize();
      } else {
        minimize();
      }
    },
  }), [isMaximized]);

  return panResponder;
};

const responseToPlaces = (response) => {
  return response.features.map((feature) => {
    return {
      placeName: feature.place_name,
      id: feature.id
    };
  });
};
const SearchResultRow = ({ placeName, id }) => {
  return (<Text
    style={styles.instructions}
    key={id}
  >
    {placeName}
  </Text>);
};

const HomeSwipeUp = ({ location, searchText, searchResults, dispatch }) => {
  // This works, don't waste requests
  // useEffect(() => {
  //   let config = {
  //     query: 'Georgia State Cap',
  //     limit: 4
  //   };
  //   if (location) {
  //     config.proximity = [location.coords.latitude, location.coords.longitude];
  //   }
  //   geocodeService.forwardGeocode(config)
  //     .send()
  //     .then(response => {
  //       const match = response.body;
  //       console.log(match);
  //     }).catch((err) => {
  //       console.error(err);
  //     });
  // }, []);
  const destinationInput = useRef(null);

  const maximizer = useMaximizer(destinationInput);
  const [swipeLength, isMaximized, maximize, minimize] = maximizer;

  const dimensions = Dimensions.get('window');
  let halfHeight = dimensions.height / 2;
  const swipeResponder = useSwipe(dimensions.height, maximizer);

  return (
    <View
      style={{
        position: 'absolute',
        top: 0,
        height: dimensions.height,
        left: 0,
        width: dimensions.width
      }}
      {...swipeResponder.panHandlers}
    >
      <Animated.View style={{
        position: 'absolute',
        top: swipeLength.interpolate({
          inputRange: [0, 1],
          outputRange: [halfHeight, 0],
        }),
        height: swipeLength.interpolate({
          inputRange: [0, 1],
          outputRange: [halfHeight, dimensions.height],
        }),
        width: dimensions.width,
        backgroundColor: palette.tabBar,
        padding: 10,
        borderTopLeftRadius: swipeLength.interpolate({
          inputRange: [-1, 0, 1],
          outputRange: [20, 20, 0]
        }),
        borderTopRightRadius: swipeLength.interpolate({
          inputRange: [-1, 0, 1],
          outputRange: [20, 20, 0]
        })
      }}>
        <ScrollView keyboardShouldPersistTaps='handled'>
          <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', paddingBottom: 10 }}>
            <Animated.View style={{
              backgroundColor: palette.inactive,
              height: 1,
              width: 50,
              opacity: swipeLength.interpolate({
                inputRange: [-1, 0, 1],
                outputRange: [20, 20, 0]
              })
            }} />
          </View>
          <ButtonInput
            inputRef={destinationInput}
            onFocus={() => {
              dispatch({ type: 'SET_SEARCH_TEXT', data: '' });
              maximize();
            }}
            onBlur={() => {
              dispatch({ type: 'SET_SEARCH_TEXT', data: 'Where to?' });
              minimize();
            }}
            icon={SolidIcons.search}
            value={searchText}
            iconPress={(value) => {
              let config = {
                query: value,
                limit: 4
              };
              if (location) {
                config.proximity = [location.coords.latitude, location.coords.longitude];
              }
              return geocodeService.forwardGeocode(config)
                .send()
                .then(response => {
                  const match = response.body;
                  // console.log(match);
                  let results = responseToPlaces(match);
                  return dispatch({ type: 'SET_SEARCH_RESULTS', data: results });
                }).catch((err) => {
                  console.error(err);
                });
            }}
            onChangeText={(newText) => {
              dispatch({ type: 'SET_SEARCH_TEXT', data: newText });
            }}
          />
          <Text style={styles.instructions}>
            Location: {location ? `(${location.coords.latitude}, ${location.coords.longitude})` : 'n/a'}
          </Text>
          {searchResults.map(SearchResultRow)}
        </ScrollView>
      </Animated.View>
    </View>
  )
};

const mapStateToProps = ({ uiReducer, locationReducer }) => ({
  sidebarOpen: uiReducer.sidebarOpen,
  searchText: uiReducer.searchText,
  searchResults: uiReducer.searchResults,
  location: locationReducer.location,
});

const mapDispatchToProps = dispatch => ({
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HomeSwipeUp);
