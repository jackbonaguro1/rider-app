import React, { useEffect, useState, useRef } from 'react';
import {
  View,
  ScrollView,
  Dimensions,
  TouchableOpacity,
} from "react-native";
import { connect } from 'react-redux';
import Text from './Text';
import styles, { palette, mapStyles } from "../styles";
import MapView from "react-native-maps";
import Button from "./Button";
import { SolidIcons, RegularIcons } from "react-native-fontawesome/FontAwesomeIcons";
import FontAwesome from "react-native-fontawesome";
import TextInput from "./TextInput";
import ButtonInput from "./ButtonInput";
import HomeSwipeUp from "./HomeSwipeUp";
import Geolocation from "react-native-geolocation-service";
import Config from "react-native-config";

const locationToCamera = (location) => {
  console.log(location);
  return location ?
    location.coords ?
      {
        center: {
          latitude: location.coords.latitude,
          longitude: location.coords.longitude,
        },
        pitch: 1,
        heading: 0,
        zoom: 13,
        altitude: 10
      }
    :
      {
        center: {
          latitude: location.latitude,
          longitude: location.longitude,
        },
        pitch: 1,
        heading: 0,
        zoom: 13,
        altitude: 10
      }
  :
    {
      center: {
        latitude: 0,
        longitude: 0,
      },
      pitch: 1,
      heading: 0,
      zoom: 1,
      altitude: 1000
    }
  ;
};

let Home = ({ sidebarOpen, location, dispatch }) => {
  // When home is loaded, fetch location from service
  useEffect(() => {
    console.log(`Config: ${JSON.stringify(Config, null, 2)}`);
    return Geolocation.getCurrentPosition(
      (pos) => {
        return dispatch({ type: 'SET_LOCATION', data: pos });
      },
      (err) => {
        console.warn(err);
      },
      { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
    );
  }, []);

  // once the location is updated, zoom the mapView there
  let mapView = useRef(null);
  useEffect(() => {
    mapView.current.animateCamera(locationToCamera(location), { duration: 1000 });
  }, [location]);

  let dimensions = Dimensions.get('window');
  return (
    <View style={styles.appContainer}>
      <MapView
        ref={mapView}
        initialCamera={locationToCamera(location)}
        style={{
          minHeight: dimensions.height,
          minWidth: dimensions.width
        }}
        customMapStyle={mapStyles}
        // I don't think we will have any use for changes to the home map itself; Uber doesn't
        // onRegionChangeComplete={() => {
          // const bounds = mapView.current.getMapBoundaries();
        // }}
      />
      <View style={{
        position: 'absolute',
        top: 0,
        left: 0,
        padding: 15
      }}
      >
        <TouchableOpacity
          style={{
            padding: 5,
            borderRadius: 5,
            backgroundColor: palette.appBackground
          }}
          onPress={() => {
            let type = sidebarOpen ? 'CLOSE_SIDEBAR' : 'OPEN_SIDEBAR';
            return dispatch({ type });
          }}
        >
          <FontAwesome icon={SolidIcons.bars}
                       color={palette.white}
                       style={[{fontSize: 20, paddingHorizontal: 6},
                         styles.instructions]}
          ></FontAwesome>
        </TouchableOpacity>
      </View>
      <View style={{
        position: 'absolute',
        top: 0,
        right: 0,
        padding: 15
      }}
      >
        <TouchableOpacity
          style={{
            padding: 5,
            borderRadius: 5,
            backgroundColor: palette.white
          }}
          onPress={() => {
            mapView.current.animateCamera(locationToCamera(location), { duration: 1000 });
          }}
        >
          <FontAwesome icon={SolidIcons.crosshairs}
                       color={palette.tabBarActive}
                       style={{
                         fontSize: 20,
                         paddingHorizontal: 6,
                         textAlign: 'center',
                         paddingVertical: 5,
                       }}
          ></FontAwesome>
        </TouchableOpacity>
      </View>
      <HomeSwipeUp></HomeSwipeUp>
    </View>
  )
};



const mapStateToProps = ({ uiReducer, locationReducer }) => ({
  sidebarOpen: uiReducer.sidebarOpen,
  location: locationReducer.location
});

const mapDispatchToProps = dispatch => ({
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home);
