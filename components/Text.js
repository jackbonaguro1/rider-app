import React from 'react';
import { Text } from 'react-native';

let CustomText = (props) => {
  return (
    <Text
      {...props}
      style={[{
        fontFamily: 'Iosevka'
      },{
        ...props.style
      }]}
    >
      {props.children}
    </Text>
  )
};

export default CustomText;
