import React from 'react';
import {
  View
} from 'react-native';
import { connect } from 'react-redux';
import Button from './Button';
import Text from './Text';
import { palette } from '../styles';

let Placeholder = ({ sidebarOpen, dispatch }) => {
  return (
    <View
      style={{
        alignItems: 'center',
        justifyContent: 'center',
        width: 100+'%',
        height: 100+'%',
        backgroundColor: palette.appBackground
      }}
    >
      <Text style={{
        color: palette.white,
        fontSize: 16,
        padding: 15
      }}>Placeholder</Text>
      <Button
        title='Menu'
        onPress={() => {
          return dispatch({ type: 'OPEN_SIDEBAR' });
        }}
      ></Button>
    </View>
  )
};


const mapStateToProps = ({ uiReducer }) => ({
  sidebarOpen: uiReducer.sidebarOpen
});

const mapDispatchToProps = dispatch => ({
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Placeholder)
