import React from 'react';
import {
  View,
  TouchableOpacity,
  StatusBar
} from 'react-native';
import { Link } from 'react-router-native';
import Text from './Text';

import styles, { palette } from "../styles";
import { connect } from "react-redux";

const Row = ({ label, route, dispatch }) => {
  return (
    <Link
      to={route}
      component={TouchableOpacity}
      onPress={() => {
        // This works but closes weirdly fast, may need to tweak animation
        return dispatch({ type: 'CLOSE_SIDEBAR' });
      }}
    >
      <View
        style={styles.sidebarRow}
      >
        <Text
          style={styles.sidebarRowText}
        >{label}</Text>
      </View>
    </Link>
  )
};

const SidebarContent = ({ dispatch }) => {
  return (
    <View style={{
      flex: 1,
    }}>
      <View style={styles.logo}>
        <Text style={styles.logoText}>Drivers.Coop</Text>
      </View>
      <View style={{
        justifyContent: 'space-between',
        flex: 1,
      }}>
        <View>
          {/*Some pages, like payment, are promoted but accessible through settings*/}
          <Row
            label={'Ride'}
            route={'/'}
            dispatch={dispatch}
          />
          <Row
            label={'History'}
            route={'/history'}
            dispatch={dispatch}
          />
          <Row
            label={'Payment'}
            route={'/settings/payment'}
            dispatch={dispatch}
          />
          <Row
            label={'About Us'}
            route={'/about'}
            dispatch={dispatch}
          />
          <Row
            label={'Help'}
            route={'/help'}
            dispatch={dispatch}
          />
          <Row
            label={'Settings'}
            route={'/settings'}
            dispatch={dispatch}
          />
        </View>
        <View style={{
          marginBottom: StatusBar.currentHeight, // TODO: Absolute positioning off?
          paddingVertical: 10,
          paddingHorizontal: 15,

          flexDirection: 'row',
          justifyContent: 'space-between'
        }}>
          <Text style={{
            color: palette.inactive
          }}>
            v0.1.0
          </Text>
          <Text style={{
            color: palette.inactive
          }}>
            #2bb2c2d8
          </Text>
        </View>
      </View>
    </View>
  );
};

const mapStateToProps = ({ uiReducer }) => ({
  sidebarOpen: uiReducer.sidebarOpen
});

const mapDispatchToProps = dispatch => ({
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SidebarContent);

