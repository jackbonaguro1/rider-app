import React, { useState, useMemo, useEffect } from 'react';
import {
  View,
  Dimensions,
  Animated,
  PanResponder,
  Pressable
} from "react-native";
import { connect } from 'react-redux';
import Text from './Text';
import SidebarContent from "./SidebarContent";
import styles, { palette } from "../styles";

const AnimatedPressable = Animated.createAnimatedComponent(Pressable);

const useOpener = (sidebarOpen) => {
  const [swipeLength, _] = useState(new Animated.Value(1));
  const [isOpen, setOpen] = useState(false);

  const open = () => {
    // console.log('Open');
    setOpen(true);
    Animated.spring(swipeLength,
      { toValue: 1, stiffness: 260, damping: 20, mass: 1, useNativeDriver: false }).start();
  }
  const close = () => {
    // console.log('Close');
    Animated.spring(swipeLength, // Lower mass since this bounce was too much
      { toValue: 0, stiffness: 260, damping: 20, mass: .5, useNativeDriver: false }).start();
    setOpen(false);
  }

  useEffect(() => {
    if (sidebarOpen) {
      open();
    } else {
      close();
    }
  }, [sidebarOpen]);

  return [swipeLength, isOpen, open, close];
  // Shouldn't typically use open / close, but I'll need them to reset
  // value of swipeLength if the gesture doesn't change isOpen
};

const useSwipe = (width, opener, dispatch) => {
  const [swipeLength, isOpen, open, close] = opener;
  const sidebarWidth = width * 2 / 3;

  const panResponder = useMemo(() => PanResponder.create({
    onMoveShouldSetPanResponder: (evt, gestureState) => {
      const threshold = isOpen ? sidebarWidth + 40 : 40;
      const inBounds = evt.nativeEvent.pageX < (threshold);

      const {dx, dy} = gestureState;
      const notTap = (Math.abs(dx) > 20) || (Math.abs(dy) > 20);

      return inBounds && notTap;
    },
    onPanResponderMove: (evt, gestureState) => {
      const offset = isOpen ? sidebarWidth : 0
      const normalized = (gestureState.dx + offset) / width * 3 / 2;
      // Cap maximum move to prevent exposing left edge
      const cappedValue = Math.min(normalized, 3 / 2);
      swipeLength.setValue(cappedValue);
    },
    onPanResponderTerminationRequest: (evt, gestureState) => true,
    onStartShouldSetPanResponderCapture: (evt, gestureState) => false,
    onPanResponderRelease: (evt, gestureState) => {
      const normalized = gestureState.dx / width; // Normalize to 0..1
      if (normalized > 0.25) { // TODO: Make comparison work bi-directionally, switch or cancel
        if (!isOpen) {
          dispatch({ type: 'OPEN_SIDEBAR' });
        } else {
          open();
        }
      } else if (normalized < -0.25) {
        if (isOpen) {
          dispatch({ type: 'CLOSE_SIDEBAR' });
        } else {
          close();
        }
      } else {
        // Too small of a swipe
        if (isOpen) {
          open();
        } else {
          close();
        }
      }
    },
    onPanResponderTerminate: (evt, gestureState) => {
      if (isOpen) {
        dispatch({type: 'OPEN_SIDEBAR'});
      } else {
        dispatch({type: 'CLOSE_SIDEBAR'});
      }
    },
  }), [isOpen]);

  return panResponder;
};

let Sidebar = ({ sidebarOpen, dispatch }) => {
  const dimensions = Dimensions.get('window');
  const sidebarWidth = dimensions.width * 2 / 3;

  const opener = useOpener(sidebarOpen);
  const [swipeLength, isOpen] = opener;

  const swipeResponder = useSwipe(dimensions.width, opener, dispatch);

  return (
    <View
      style={{
        position: 'absolute',
        top: 0,
        height: dimensions.height,
        left: 0,
        width: dimensions.width,
        // backgroundColor: '#ff08'
      }}
      {...swipeResponder.panHandlers}
    >
      <AnimatedPressable
        style={{
          position: 'absolute',
          top: 0,
          height: dimensions.height,
          left: sidebarOpen ? 0 : -dimensions.width,
          width: dimensions.width,
          backgroundColor: '#070510',
          opacity: swipeLength.interpolate({
            inputRange: [0, 1],
            outputRange: [0, 0.75]
          })
        }}
        android_disableSound={true}
        onPressIn={(pressEvent) => {
          // TODO: differentiate taps / swipes and only close on tap (Lyft)
          // console.log(pressEvent);
          let type = sidebarOpen ? 'CLOSE_SIDEBAR' : 'OPEN_SIDEBAR';
          return dispatch({ type });
        }}
      >
        <></>
      </AnimatedPressable>
      <Animated.View style={{
        position: 'absolute',
        top: 0,
        height: dimensions.height,
        left: swipeLength.interpolate({
          inputRange: [0, 1],
          outputRange: [-dimensions.width, -sidebarWidth / 2]
        }),
        width: dimensions.width, // slightly wider due to spring
        backgroundColor: palette.appBackground,
        flexDirection: 'row'
      }}>
        <View style={{
          width: sidebarWidth / 2,
          // backgroundColor: '#ff888888'
        }}/>
        <View style={{
          width: sidebarWidth,
          // backgroundColor: '#8f88'
        }}>
          {/* The actual sidebar starts here */}
          <SidebarContent></SidebarContent>
        </View>
      </Animated.View>
    </View>
  )
};


const mapStateToProps = ({ uiReducer }) => ({
  sidebarOpen: uiReducer.sidebarOpen
});

const mapDispatchToProps = dispatch => ({
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Sidebar)
