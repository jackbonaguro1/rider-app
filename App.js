/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useEffect } from 'react';
import {
  SafeAreaView,
  StatusBar,
  View
} from 'react-native';
import { Provider } from 'react-redux';
import store from './store';
import { createStore } from 'redux';
import { NativeRouter, Route, Redirect, Switch } from 'react-router-native';

import Text from './components/Text';
import Home from './components/Home';
import MapView from 'react-native-maps';
import Sidebar from "./components/Sidebar";
import Button from "./components/Button";
import Placeholder from "./components/Placeholder";

const App = ({ location, dispatch }) => {
  return (
    <Provider store={store}>
      <NativeRouter>
        <StatusBar barStyle="dark-content" />
        <SafeAreaView>
          <Switch style={{flex:1}}>
            <Redirect exact from="/" to="/home" />
            <Route
              path="/home"
              render={(props) => (
                <Home {...props} />
              )}
            />
            <Route
              path='/placeholder'
            >
              <Placeholder/>
            </Route>
            <Redirect exact from='/history' to='/placeholder' />
            <Redirect exact from='/settings/payment' to='/placeholder' />
            <Redirect exact from='/about' to='/placeholder' />
            <Redirect exact from='/help' to='/placeholder' />
            <Redirect exact from='/settings' to='/placeholder' />
          </Switch>
          <Sidebar/>
        </SafeAreaView>
      </NativeRouter>
    </Provider>
  );
};

export default App;
