import { combineReducers } from 'redux';

const defaultUIState = {
  sidebarOpen: false,
  searchText: 'Where to?',
  searchResults: [],
}

const uiReducer = (state = defaultUIState, action) => {
  switch (action.type) {
    case 'OPEN_SIDEBAR': {
      return {
        ...state,
        sidebarOpen: true
      };
    }
    case 'CLOSE_SIDEBAR': {
      return {
        ...state,
        sidebarOpen: false
      };
    }
    case 'SET_SEARCH_TEXT': {
      return {
        ...state,
        searchText: action.data
      };
    }
    case 'SET_SEARCH_RESULTS': {
      return {
        ...state,
        searchResults: action.data
      };
    }
    default: {
      return state;
    }
  }
};

const locationReducer = (state = {}, action) => {
  switch (action.type) {
    case 'SET_LOCATION': {
      // console.warn(action.data);
      return {
        ...state,
        location: action.data
      };
    }
    default: {
      return state;
    }
  }
};

export default combineReducers({
  uiReducer,
  locationReducer
});
