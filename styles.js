import {
  StyleSheet,
  Dimensions,
  StatusBar,
} from 'react-native';

const palette = {
  white: '#FFFFFF',
  tabBar: '#383838',
  tabBarActive: '#880088',
  inactive: '#888888',
  inputBackground: '#FFFFFF44',
  black: '#000000',
  transparent: '#00000000',
  appBackground: '#444444',
  purple: '#880088',
};

const styles = StyleSheet.create({
  scrollContainer: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  container: {
    width: 100+'%',
  },
  title: {
    fontSize: 20,
    textAlign: 'center',
    color: palette.white,
    margin: 10,
  },
  logo: {
    padding: 25,
    // borderColor: palette.white,
    // borderWidth: 1,
    backgroundColor: palette.appBackground,
    marginBottom: 10,
    // courtesy of:
    // https://ethercreative.github.io/react-native-shadow-generator/
    // shadowColor: "#f8f",
    // shadowOffset: {
    //   width: 0,
    //   height: 2,
    // },
    // shadowOpacity: 0.25,
    // shadowRadius: 3.84,
    //
    // elevation: 5,
  },
  logoText: {
    fontSize: 20,
    fontFamily: 'Iosevka-Semibold',
    textAlign: 'center',
    color: palette.white,
  },
  instructions: {
    textAlign: 'center',
    color: palette.white,
    paddingVertical: 5,
  },
  routerButton: {
    padding: 10,
    borderRadius: 10,
    backgroundColor: '#555555',
    color: palette.white,
    flex: 0,
  },
  tabBar: {
    backgroundColor: palette.tabBar,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    minHeight: 55,
  },
  tabBarLink: {
    paddingHorizontal: 10,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  tabBarButton: {
    color: palette.inactive,
    fontSize: 24,
    paddingVertical: 10,
    paddingHorizontal: 20,
    borderBottomColor: palette.tabBar,
    borderTopColor: palette.tabBar,
    borderBottomWidth: 2,
    borderTopWidth: 2,
  },
  tabBarButtonActive: {
    color: palette.white,
    fontSize: 24,
    paddingVertical: 10,
    paddingHorizontal: 20,
    borderBottomColor: palette.tabBar,
    borderTopColor: palette.tabBar,
    borderBottomWidth: 2,
    borderTopWidth: 2,
  },
  appContainer: {
    height: Dimensions.get('window').height - StatusBar.currentHeight,
    backgroundColor: palette.appBackground,
    flexDirection: 'column',
    alignItems: 'flex-end',
  },
  sidebarRow: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    paddingHorizontal: 15,
    paddingVertical: 10,
  },
  sidebarRowText: {
    color: palette.white,
    fontSize: 16
  }
});

const mapStyles = [
  {
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#212121"
      }
    ]
  },
  {
    "elementType": "labels.icon",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "elementType": "labels.text.stroke",
    "stylers": [
      {
        "color": "#212121"
      }
    ]
  },
  {
    "featureType": "administrative",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "featureType": "administrative.country",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  },
  {
    "featureType": "administrative.land_parcel",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "administrative.locality",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#bdbdbd"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#181818"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#616161"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "labels.text.stroke",
    "stylers": [
      {
        "color": "#1b1b1b"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "geometry.fill",
    "stylers": [
      {
        "color": "#2c2c2c"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#8a8a8a"
      }
    ]
  },
  {
    "featureType": "road.arterial",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#373737"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#3c3c3c"
      }
    ]
  },
  {
    "featureType": "road.highway.controlled_access",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#4e4e4e"
      }
    ]
  },
  {
    "featureType": "road.local",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#616161"
      }
    ]
  },
  {
    "featureType": "transit",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#000000"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#3d3d3d"
      }
    ]
  }
];

export default styles;
export { palette, mapStyles };
